package com.ftpclient;

public class Constant {

    public static String MY_FTP = "myftp> ";
    
    public static String HOST_NAME ="inet.cis.fiu.edu";;//"";
    public static String HOST_IP =  "131.94.130.139";
    public static int HOST_PORT = 21;
    
    public static String HOST_USER_NAME = "demo";
    
    public static String HOST_USER_PASSWORD= "demopass";

    public static String CMD_LS = "ls";
    public static String CMD_GET = "get";
    public static String CMD_PUT = "put";
    public static String CMD_DELETE = "delete";
    public static String CMD_QUIT = "quit ";

}
