package com.ftpclient;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Artur Hovhannisyan on 10/12/2015.
 */
public class FTPClientConnectionView extends Constant {

    public static void main(String[] args) {

        FTPClientConnection ftp = new FTPClientConnection(true);
        Scanner sc = new Scanner(System.in); 
        String commandInline = "";


        System.out.println("Please entry the server name: ");
        System.out.print(MY_FTP);
        

        boolean nameEntered = false;

        while (true) {

            if (sc.hasNextLine()) {
                commandInline = sc.nextLine();
                if (!Util.isEmpty(commandInline)) {
                     
                    //try to connect  
                    if (!ftp.isConnected()) {
                        String serverName = commandInline.trim();

                        try {
                            if (ftp.connect(serverName)) {
                                ftp.setConnected(true);
                                System.out.printf("Please type in your username: \r");
                            } else {
                                System.out.println(String.format("Failure : Please try again, Invalid server name  ", serverName));
                            }
                        } catch (IOException e) {
                            System.out.println(String.format("Failure : Please try again, Invalid server name  ", serverName));
                        }
                    } else {

                        if (ftp.isLoggedIn()) {
                            String command = commandInline.trim();
                            System.out.println(String.format("command = [%s]" , command));
                            try {

                                String currentDir = ftp.getCurrentDirectory();
                                if (CMD_LS.equalsIgnoreCase(command)) {
                                    String listFiles = ftp.listFiles();
                                    System.out.println(listFiles);
                                    System.out.println(String.format("Success : "));
                                } else if (command.startsWith(CMD_GET)) {
                                    command = command.replace(CMD_GET,"");
                                    command = command.trim();
                                    if(command != null && command.length() > 0){
                                        if(ftp.downloadFile(command)){
                                            System.out.println(String.format("Success : "));
                                        } else {
                                            System.out.println(String.format("Failure : Please entry correct file name; File not exist [%s]", command));
                                        }
                                    } else {
                                        System.out.println(String.format("Failure : Please entry correct file name "));
                                    }

                                } else if (command.startsWith(CMD_PUT)) {
                                    command = command.replace(CMD_PUT,"");
                                    command = command.trim();
                                    if(command != null && command.length() > 0){
                                        File file = new File(command);
                                        if (file.exists() && !file.isFile()){
                                            if(ftp.uploadFile(currentDir, file.getAbsolutePath())){
                                                System.out.println(String.format("Success : "));
                                            } else {
                                                System.out.println(String.format("Failure : Please entry correct file name; File not exist [%s]", command));
                                            }
                                        } else {
                                            System.out.println(String.format("Failure : "));
                                        }
                                    } else {
                                        System.out.println(String.format("Failure : Please entry correct file name "));
                                    }
                                } else if (command.startsWith(CMD_DELETE)) {
                                    command = command.replace(CMD_DELETE,"");
                                    command = command.trim();
                                    if(command != null && command.length() > 0){
                                        if(ftp.deleteFile( command)){
                                            System.out.println(String.format("Success : "));
                                        } else {
                                            System.out.println(String.format("Failure : Please entry correct file name; File not exist [%s]", command));
                                        }
                                    } else {
                                        System.out.println(String.format("Failure : Please entry correct file name "));
                                    }
                                } else if (CMD_QUIT.equalsIgnoreCase(command)) {
                                    ftp.disconnect();
                                    System.out.println(String.format("Success : You disconnected successfully"));
                                } else {
                                    System.out.println(String.format("Failure : Please try again, Invalid command [%s] ", command));
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                System.out.println(String.format("Failure"));
                            } finally {
                                System.out.print(MY_FTP);
                            }
                        } else {
                            if (nameEntered) {

                                String psw = commandInline.trim();
                                try {
                                    if (ftp.entryUserPassword(psw)) {
                                        nameEntered = false;
                                        ftp.setLoggedIn(true);
                                        System.out.println(String.format("Success"));
                                        System.out.println("Login successful");
                                        System.out.print(MY_FTP);
                                    } else {
                                        System.out.println(String.format("Failure"));
                                        System.out.print("Please type in your username :");
                                    }

                                } catch (IOException e) {
                                    System.out.println(String.format("Failure"));
                                    System.out.print("Please type in your username :");
                                }
                            } else {

                                String userName = commandInline.trim();
                                try {
                                    if (ftp.entryUserName(userName)) {
                                        nameEntered = true;
                                        System.out.print("Please type in your password :");
                                    } else {
                                        nameEntered = false;
                                        System.out.println(String.format("Failure"));
                                        System.out.print("Please type in your username :");
                                    }
                                } catch (IOException e) {
                                    System.out.println(String.format("Failure"));
                                    System.out.print("Please type in your username :");
                                }
                            }
                        }
                    }                      
                }
            }
        } 
    }
    
}
